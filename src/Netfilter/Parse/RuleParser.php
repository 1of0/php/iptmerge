<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Parse;

use OneOfZero\IptMerge\Exception\InvalidDefinitionException;
use OneOfZero\IptMerge\Exception\InvalidActionException;
use OneOfZero\IptMerge\Exception\UnexpectedTokenException;
use OneOfZero\IptMerge\Netfilter\Structure\Rule;
use OneOfZero\IptMerge\Netfilter\Structure\RuleOption;
use OneOfZero\IptMerge\Netfilter\Structure\RuleOptionCollection;

class RuleParser
{
    private const OPTION_ALIASES = [
        'A' => ['append'],
        'I' => ['insert'],
        'D' => ['delete'],
        'N' => ['new-chain'],
        'X' => ['delete-chain'],
        'F' => ['flush'],
        's' => ['src', 'source'],
        'd' => ['dst', 'destination'],
    ];

    private const CHAIN_ACTION_OPTIONS = ['A', 'I', 'D', 'N', 'X', 'F'];

    private const EXPANDING_OPTIONS = [ 's', 'd' ];

    private const NON_REPEATABLE_OPTIONS = [
        ...self::EXPANDING_OPTIONS,
        'comment'
    ];

    private const SINGLE_VALUE_OPTIONS = [
        ...self::CHAIN_ACTION_OPTIONS,
        ...self::EXPANDING_OPTIONS,
        'comment'
    ];

    private const PATTERN_OPTION_NAME = /** @lang PhpRegExp */ '/^--?(.+)$/';

    /**
     * @param string $ruleDefinition
     * @return Rule[]
     */
    public function parse(string $ruleDefinition): array
    {
        $tokenizer = new RuleTokenizer();

        $parsedOptions = [];

        $groupedTokens = $tokenizer->groupTokens($tokenizer->tokenize($ruleDefinition));
        foreach ($groupedTokens as $tokenGroup) {
            $negated = false;
            if ($tokenGroup[0] === RuleTokenizer::NEGATION_TOKEN) {
                $negated = true;
                array_shift($tokenGroup);
            }

            $matches = [];
            if (!preg_match(self::PATTERN_OPTION_NAME, $tokenGroup[0] ?? '', $matches)) {
                $message = sprintf(
                    "Could not determine option name for token group.\n\nRule: %s\nToken group:\n%s\n",
                    $ruleDefinition,
                    print_r($tokenGroup, true)
                );
                throw new UnexpectedTokenException($message);
            }

            [, $name] = $matches;
            array_shift($tokenGroup);

            // Rest of the tokens in the group should be arguments for the option
            $values = $tokenGroup;

            $parsedOptions[] = new RuleOption($name, $values, $negated);
        }

        $options = new RuleOptionCollection($parsedOptions);

        $this->validateOptions($ruleDefinition, $options);

        /** @var RuleOption $actionOption */
        $actionOption = $options->getFirstOptionByAnyName($this->resolveAliases(self::CHAIN_ACTION_OPTIONS));

        $rule = new Rule(
            $actionOption->getName(),
            $actionOption->getValue(0),
            $options->withoutOption($actionOption)->getOptions()
        );

        return $this->expandMultiValueOptions($rule);
    }

    private function validateOptions(string $ruleDefinition, RuleOptionCollection $options): void
    {
        $actions = $options->getOptionsByAnyName($this->resolveAliases(self::CHAIN_ACTION_OPTIONS));

        if (count($actions) !== 1) {
            throw new InvalidActionException(
                "Rule does not contain exactly one chain action (e.g. -I INPUT).\n\n" .
                "Rule: {$ruleDefinition}\n"
            );
        }

        foreach (self::NON_REPEATABLE_OPTIONS as $nonRepeatableOption) {
            $foundOptions = $options->getOptionsByAnyName($this->resolveAliases([$nonRepeatableOption]));
            if (count($foundOptions) > 1) {
                throw new InvalidDefinitionException(
                    "Option {$nonRepeatableOption} may only be provided once for every definition.\n\n" .
                    "Rule: {$ruleDefinition}\n"
                );
            }
        }

        $singleValueOptions = $options->getOptionsByAnyName($this->resolveAliases(self::SINGLE_VALUE_OPTIONS));
        foreach ($singleValueOptions as $singleValueOption) {
            if (count($singleValueOption->getValues()) !== 1) {
                throw new InvalidDefinitionException(
                    "Option {$singleValueOption->getName()} must have exactly one value.\n\n" .
                    "Rule: {$ruleDefinition}\n"
                );
            }
        }
    }

    /**
     * @param Rule $rule
     * @return Rule[]
     */
    private function expandMultiValueOptions(Rule $rule): array
    {
        $rules = [];

        foreach (self::EXPANDING_OPTIONS as $expandingOption) {
            $option = $rule->getFirstOptionByAnyName($this->resolveAliases([$expandingOption]));

            if ($option === null) {
                continue;
            }

            $optionName = $option->getName();
            $value      = $option->getValue(0);

            if ($value === null || !str_contains($value, ',')) {
                continue;
            }

            foreach (explode(',', $value) as $item) {
                $itemOption = new RuleOption($optionName, [$item], $option->isNegated());
                $rules[] = $rule->withReplacedOption($optionName, $itemOption);
            }
        }

        return $rules ?: [$rule];
    }

    private function resolveAliases(array $optionNames): array
    {
        $resolved = $optionNames;

        foreach ($optionNames as $optionName) {
            $aliases = self::OPTION_ALIASES[$optionName] ?? [];
            foreach ($aliases as $alias) {
                $resolved[] = $alias;
            }
        }

        return $resolved;
    }
}
