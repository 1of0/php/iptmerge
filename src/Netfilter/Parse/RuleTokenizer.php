<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Parse;

use OneOfZero\IptMerge\Exception\UnclosedStringException;

class RuleTokenizer
{
    private const PATTERN_ANY_WHITESPACE              = /** @lang PhpRegExp */ '/^(\s+)/';
    private const PATTERN_QUOTED_STRING_START         = /** @lang PhpRegExp */ '/^(["\']).*/';
    private const PATTERN_SHORT_OPTION                = /** @lang PhpRegExp */ '/^(-[^-\'"\s])/';
    private const PATTERN_LONG_OPTION_WITH_ASSIGNMENT = /** @lang PhpRegExp */ '/^(--[^\'"\s]+?=)/';
    private const ANYTHING_UNTIL_DELIMITER            = /** @lang PhpRegExp */ '/^(.+?)(?:[\'"\s]|$)/';

    public const NEGATION_TOKEN = '!';

    public function tokenize(string $definition): array
    {
        $position = 0;
        $length   = strlen($definition);
        $tokens   = [];

        while ($position < $length) {
            $chunk = substr($definition, $position);

            $whitespace = $this->findWhitespace($chunk);
            if ($whitespace !== null) {
                $position += strlen($whitespace);
                continue;
            }

            $token = $this->findQuotedString($chunk);
            if ($token !== null) {
                $tokens[] = StringHelper::unescape($token);
                $position += strlen($token);
                continue;
            }

            $token = $this->findOptionWithAssignment($chunk);
            if ($token !== null) {
                $tokens[] = rtrim($token, '=');
                $position += strlen($token);
                continue;
            }

            $token =
                $this->findShortOption($chunk) ??
                $this->findAnythingUntilDelimiter($chunk);

            $tokens[] = $token;
            $position += strlen($token);
        }

        return $tokens;
    }

    public function groupTokens(array $tokens): array
    {
        $groups       = [];
        $currentGroup = [];

        foreach ($tokens as $token) {
            $newGroup = false;

            if ($currentGroup !== [self::NEGATION_TOKEN] && str_starts_with($token, '-')) {
                $newGroup = true;
            }

            if ($token === self::NEGATION_TOKEN) {
                $newGroup = true;
            }

            if ($newGroup) {
                if ($currentGroup) {
                    $groups[] = $currentGroup;
                }
                $currentGroup = [];
            }

            $currentGroup[] = $token;
        }

        if ($currentGroup) {
            $groups[] = $currentGroup;
        }

        return $groups;
    }

    private function findWhitespace(string $chunk): ?string
    {
        $matches = [];
        if (preg_match(self::PATTERN_ANY_WHITESPACE, $chunk, $matches)) {
            return $matches[1];
        }
        return null;
    }

    private function findQuotedString(string $chunk): ?string
    {
        $matches = [];
        if (!preg_match(self::PATTERN_QUOTED_STRING_START, $chunk, $matches)) {
            return null;
        }
        [, $quoteChar] = $matches;

        $length     = strlen($chunk);
        $isEscaping = false;

        for ($i = 1; $i < $length; $i++) {
            if ($isEscaping) {
                $isEscaping = false;
                // We're not interested in escaped characters
                continue;
            }

            if ($chunk[$i] === '\\') {
                $isEscaping = true;
                continue;
            }

            if ($chunk[$i] === $quoteChar) {
                return substr($chunk, 0, $i + 1);
            }
        }

        throw new UnclosedStringException("Could not find closing quote for string in chunk: {$chunk}");
    }

    private function findShortOption(string $chunk): ?string
    {
        // Starts with '-' followed by any character that is not a space, quote, or dash
        if (preg_match(self::PATTERN_SHORT_OPTION, $chunk, $matches)) {
            return $matches[1];
        }
        return null;
    }

    private function findOptionWithAssignment(string $chunk): ?string
    {
        // Starts with '--' followed by any number of characters that are not spaces or quotes, followed by '='
        if (preg_match(self::PATTERN_LONG_OPTION_WITH_ASSIGNMENT, $chunk, $matches)) {
            return $matches[1];
        }
        return null;
    }

    private function findAnythingUntilDelimiter(string $chunk): string
    {
        $matches = [];
        // Any number of characters that are not spaces or quotes until we hit a space or quote or the end of the string
        preg_match(self::ANYTHING_UNTIL_DELIMITER, $chunk, $matches);
        return $matches[1];
    }
}
