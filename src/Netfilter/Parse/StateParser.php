<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Parse;

use Ds\Set;
use OneOfZero\IptMerge\Exception\InvalidDefinitionException;
use OneOfZero\IptMerge\Netfilter\Structure\Chain;
use OneOfZero\IptMerge\Netfilter\Structure\State;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

class StateParser
{
    private const PATTERN_COMMENT =
        /** @lang PhpRegExp */
        '/^\s*#/';

    private const PATTERN_TABLE =
        /** @lang PhpRegExp */
        '/^\*(.*)/';

    private const PATTERN_CHAIN =
        /** @lang PhpRegExp @noinspection RegExpRedundantEscape */
        '/^:(.*?)\s+(.*?)\s+(\[\d+:\d+\])/';

    private const PATTERN_COMMIT =
        /** @lang PhpRegExp */
        '/^\s*COMMIT\s*$/';

    private RuleParser $ruleParser;

    public function __construct(?RuleParser $ruleParser = null)
    {
        $this->ruleParser = $ruleParser ?? new RuleParser();
    }

    public function parse(string $content): State
    {
        $table       = null;
        $tableChains = [];
        $tableRules  = [];

        $tables = [];
        foreach (explode("\n", $content) as $line) {
            if (!trim($line) || preg_match(self::PATTERN_COMMENT, $line)) {
                continue;
            }

            $matches = [];
            if (preg_match(self::PATTERN_TABLE, $line, $matches)) {
                [, $table] = $matches;
                continue;
            }

            if ($table === null) {
                throw new InvalidDefinitionException('Found definition outside table scope');
            }

            if (preg_match(self::PATTERN_CHAIN, $line, $matches)) {
                [, $name, $policy, $counters] = $matches;
                $tableChains[] = new Chain($name, $policy, $counters);
                continue;
            }

            if (preg_match(self::PATTERN_COMMIT, $line)) {
                $tables[] = new Table($table, new Set($tableChains), new Set($tableRules));

                $table       = null;
                $tableChains = [];
                $tableRules  = [];
                continue;
            }

            foreach ($this->ruleParser->parse($line) as $rule) {
                $tableRules[] = $rule;
            }
        }

        return new State($tables);
    }
}
