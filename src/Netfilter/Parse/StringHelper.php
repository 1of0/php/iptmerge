<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Parse;

class StringHelper
{
    public const ESCAPE_MAP = [
        '\\' => '\\\\',
        '"'  => '\\"',
        "'"  => "\\'",
    ];

    private const PATTERN_QUOTED_STRING_CONTENTS = /** @lang PhpRegExp */ '/^([\'"])(.*)([\'\"])$/';

    public static function escape(string $input): string
    {
        $escaped = str_replace(
            array_keys(self::ESCAPE_MAP),
            array_values(self::ESCAPE_MAP),
            $input
        );
        return "'{$escaped}'";
    }

    public static function unescape(string $input): string
    {
        $matches = [];
        if (preg_match(self::PATTERN_QUOTED_STRING_CONTENTS, $input, $matches)) {
            [, $opening, $content, $closing] = $matches;
            if ($opening === $closing) {
                $input = $content;
            }
        }

        return str_replace(
            array_values(self::ESCAPE_MAP),
            array_keys(self::ESCAPE_MAP),
            $input
        );
    }
}
