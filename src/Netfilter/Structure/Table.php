<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Structure;

use Ds\Map;
use Ds\Set;

class Table
{
    private string $name;

    /**
     * @var Set<Chain>
     */
    private Set $chains;

    /**
     * @var Set<Rule>
     */
    private Set $rules;

    public function __construct(string $name, Set $chains, Set $rules)
    {
        $this->name   = $name;
        $this->chains = $chains;
        $this->rules  = $rules;
    }

    public function __debugInfo()
    {
        return [
            'name'   => $this->name,
            'chains' => $this->chains->toArray(),
            'rules'  => $this->rules->toArray(),
        ];
    }

    public function __toString(): string
    {
        $chains = implode("\n", array_map('strval', $this->chains->toArray()));
        $rules  = implode("\n", array_map('strval', $this->rules->toArray()));

        return sprintf(
            "*%s\n%s%sCOMMIT\n",
            $this->name,
            $chains ? "{$chains}\n" : '',
            $rules  ? "{$rules}\n"  : '',
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getChains(): Set
    {
        return $this->chains;
    }

    public function getChainsIndexedByName(): Map
    {
        $map = new Map();

        /** @var Chain $chain */
        foreach ($this->chains as $chain) {
            $map->put($chain->getName(), $chain);
        }

        return $map;
    }

    public function getRules(): Set
    {
        return $this->rules;
    }
}
