<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Structure;

class State
{
    /**
     * @var Table[]
     */
    private array $tables = [];

    /**
     * @param Table[] $tables
     */
    public function __construct(array $tables = [])
    {
        foreach ($tables as $table) {
            $this->tables[$table->getName()] = $table;
        }
    }

    /**
     * @return array<string, Table>
     */
    public function getTablesIndexedByName(): array
    {
        return $this->tables;
    }

    public function __toString(): string
    {
        return implode('', array_map('strval', $this->tables));
    }
}
