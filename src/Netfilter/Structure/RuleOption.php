<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Structure;

use Ds\Hashable;
use OneOfZero\IptMerge\Netfilter\Parse\StringHelper;
use OneOfZero\IpUtils\Factory;
use OneOfZero\IpUtils\NetworkLocationInterface;

class RuleOption implements Hashable
{
    private string $name;

    /**
     * @var string[]
     */
    private array $values;

    private bool $negated;

    /**
     * @param string   $name
     * @param string[] $values
     * @param bool     $negated
     */
    public function __construct(string $name, array $values = [], bool $negated = false)
    {
        $this->name    = $name;
        $this->values  = $this->canonicalizeValues($values);
        $this->negated = $negated;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(int $index, ?string $default = null): ?string
    {
        return $this->values[$index] ?? $default;
    }

    /**
     * @return string[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    public function isNegated(): bool
    {
        return $this->negated;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        return array_merge(
            $this->negated ? ['!'] : [],
            [$this->getOptionArgument()],
            $this->getEscapedValues()
        );
    }

    private function getOptionArgument(): string
    {
        return strlen($this->name) > 1
            ? "--{$this->name}"
            : "-{$this->name}";
    }

    public function __toString(): string
    {
        return implode(' ', $this->toArray());
    }

    private function getEscapedValues(): array
    {
        return array_map([StringHelper::class, 'escape'], $this->values);
    }

    private function canonicalizeValues(array $values): array
    {
        $canonicalValues = [];
        foreach ($values as $value) {
            $parsed = Factory::get()->parse($value) ?? $value;

            if ($parsed instanceof NetworkLocationInterface) {
                $parsed = $parsed->getCidr();
            }

            $canonicalValues[] = $parsed;
        }
        return $canonicalValues;
    }

    public function hash(): string
    {
        return md5((string)$this);
    }

    /**
     * @param object $other
     * @return bool
     */
    public function equals($other): bool
    {
        return $other instanceof self
            && $other->name === $this->name
            && $other->negated === $this->negated
            && $other->values === $this->values;
    }
}
