<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Structure;

use Ds\Hashable;

class Chain implements Hashable
{
    private string $name;

    private string $policy;

    private string $counters;

    public function __construct(string $name, string $policy, string $counters)
    {
        $this->name     = $name;
        $this->policy   = $policy;
        $this->counters = $counters;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPolicy(): string
    {
        return $this->policy;
    }

    public function getCounters(): string
    {
        return $this->counters;
    }

    public function withCounters(string $counters): self
    {
        $clone = clone $this;
        $clone->counters = $counters;
        return $clone;
    }

    public function __toString(): string
    {
        return ":{$this->name} {$this->policy} {$this->counters}";
    }

    public function hash(): string
    {
        return md5($this->name);
    }

    /**
     * @param object $other
     * @return bool
     */
    public function equals($other): bool
    {
        return $other instanceof self
            && $other->name === $this->name;
    }
}
