<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Structure;

class RuleOptionCollection
{
    /**
     * @var RuleOption[]
     */
    protected array $options;

    public function __construct(array $options = [])
    {
        $this->options = array_values($options);
    }

    /**
     * @return RuleOption[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param string $optionName
     * @return RuleOption[]
     */
    public function getOptionsByName(string $optionName): array
    {
        return array_values(array_filter(
            $this->options,
            static function (RuleOption $item) use ($optionName): bool {
                return $item->getName() === $optionName;
            }
        ));
    }

    /**
     * @param string[] $optionNames
     * @return RuleOption[]
     */
    public function getOptionsByAnyName(array $optionNames): array
    {
        return array_values(array_filter(
            $this->options,
            static function (RuleOption $item) use ($optionNames): bool {
                return in_array($item->getName(), $optionNames, true);
            }
        ));
    }

    public function getFirstOptionByName(string $optionName): ?RuleOption
    {
        return $this->getOptionsByName($optionName)[0] ?? null;
    }

    /**
     * @param string[] $optionNames
     * @return RuleOption|null
     */
    public function getFirstOptionByAnyName(array $optionNames): ?RuleOption
    {
        return $this->getOptionsByAnyName($optionNames)[0] ?? null;
    }

    public function withReplacedOption(string $optionName, RuleOption $replacementOption): self
    {
        $clone = clone $this;

        foreach ($clone->options as $key => $option) {
            if ($option->getName() === $optionName) {
                $clone->options[$key] = $replacementOption;
            }
        }

        return $clone;
    }

    public function withoutOption(RuleOption $optionToRemove): self
    {
        $clone = clone $this;

        $clone->options = array_values(array_filter(
            $clone->options,
            static function (RuleOption $item) use ($optionToRemove): bool {
                return !$item->equals($optionToRemove);
            }
        ));

        return $clone;
    }
}
