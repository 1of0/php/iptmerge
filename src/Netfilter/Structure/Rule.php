<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter\Structure;

use Ds\Hashable;
use Ds\Set;

class Rule extends RuleOptionCollection implements Hashable
{
    private string $action;

    private string $chain;

    /**
     * @param string   $action
     * @param string   $chain
     * @param RuleOption[] $options
     */
    public function __construct(string $action = '', string $chain = '', array $options = [])
    {
        $this->action = $action;
        $this->chain  = $chain;
        parent::__construct($options);
    }

    public function __toString(): string
    {
        return implode(' ', array_map('strval', [$this->getActionOption(), ...$this->options]));
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getChain(): string
    {
        return $this->chain;
    }

    public function withAction(string $action): self
    {
        $clone = clone $this;
        $clone->action = $action;
        return $clone;
    }

    private function getActionOption(): string
    {
        return new RuleOption($this->action, [$this->chain]);
    }

    public function hash(): string
    {
        $options = array_map('strval', $this->options);
        sort($options);
        return md5($this->chain . '::' . implode(' ', $options));
    }

    /**
     * @param object $other
     * @return bool
     */
    public function equals($other): bool
    {
        return $other instanceof self
            && $other->chain === $this->chain
            && (new Set($other->options))->diff(new Set($this->options))->isEmpty();
    }
}
