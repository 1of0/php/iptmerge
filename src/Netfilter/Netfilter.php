<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Netfilter;

use OneOfZero\IptMerge\Exception\ParseException;
use OneOfZero\IptMerge\Netfilter\Parse\StateParser;
use OneOfZero\IptMerge\Netfilter\Structure\State;
use Symfony\Component\Finder\SplFileInfo;

class Netfilter
{
    private StateParser $stateParser;

    public function __construct(?StateParser $stateParser = null)
    {
        if ($stateParser === null) {
            $stateParser = new StateParser();
        }

        $this->stateParser = $stateParser;
    }

    public function parseStateFromFile(string $path): State
    {
        return $this->stateParser->parse($this->readFile($path));
    }

    public function parseStateFromString(string $state): State
    {
        return $this->stateParser->parse($state);
    }

    private function readFile(string $path): string
    {
        $fileInfo = new SplFileInfo($path, getcwd(), $path);

        if (!$fileInfo->isFile()) {
            throw new ParseException("Path '{$path}' is not a file");
        }

        if (!$fileInfo->isReadable()) {
            // Covered by NetfilterTest::testUnreadable(), but test won't work when run as root (e.g. in a pipeline)
            // @codeCoverageIgnoreStart
            throw new ParseException("File '{$path}' cannot be read");
            // @codeCoverageIgnoreEnd
        }

        return $fileInfo->getContents();
    }
}
