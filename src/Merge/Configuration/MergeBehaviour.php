<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Configuration;

class MergeBehaviour
{
    private Managed $managed;

    private Unmanaged $unmanaged;

    protected function __construct()
    {
    }

    public static function fromArray(array $settings): self
    {
        $instance = new self;

        $instance->managed   = Managed::fromArray($settings['managed'] ?? []);
        $instance->unmanaged = Unmanaged::fromArray($settings['unmanaged'] ?? []);

        return $instance;
    }

    public function getManaged(): Managed
    {
        return $this->managed;
    }

    public function getUnmanaged(): Unmanaged
    {
        return $this->unmanaged;
    }
}
