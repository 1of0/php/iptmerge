<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Configuration;

class Managed
{
    private string $chainPattern;

    private string $rulePattern;

    protected function __construct()
    {
    }

    public static function fromArray(array $settings): self
    {
        $instance = new self;
        $instance->chainPattern = $settings['chainPattern'] ?? '';
        $instance->rulePattern  = $settings['rulePattern'] ?? '';
        return $instance;
    }

    public function getChainPattern(): string
    {
        return $this->chainPattern;
    }

    public function getRulePattern(): string
    {
        return $this->rulePattern;
    }
}
