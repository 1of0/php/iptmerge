<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Configuration;

use OneOfZero\IptMerge\Exception\InvalidConfigurationException;

class Unmanaged
{
    public const STRATEGY_PURGE     = 'purge';
    public const STRATEGY_WHITELIST = 'whitelist';
    public const STRATEGY_KEEP      = 'keep';
    public const MATCH_COMMENT      = 'comment';
    public const MATCH_FULL         = 'full';

    private const STRATEGY_OPTIONS = [
        self::STRATEGY_PURGE,
        self::STRATEGY_WHITELIST,
        self::STRATEGY_KEEP,
    ];

    private const MATCH_OPTIONS = [
        self::MATCH_COMMENT,
        self::MATCH_FULL,
    ];

    private const PROPERTY_ENUM_MAPPING = [
        'chainStrategy'    => self::STRATEGY_OPTIONS,
        'ruleStrategy'     => self::STRATEGY_OPTIONS,
        'ruleMatchAgainst' => self::MATCH_OPTIONS,
    ];

    private string $chainStrategy = self::STRATEGY_KEEP;

    private string $ruleStrategy = self::STRATEGY_KEEP;

    private string $ruleMatchAgainst = self::MATCH_COMMENT;

    /**
     * @var string[]
     */
    private array $chainWhitelists = [];

    /**
     * @var string[]
     */
    private array $ruleWhitelists = [];

    protected function __construct()
    {
    }

    public static function fromArray(array $settings): self
    {
        $instance = new self;

        foreach (array_keys(get_object_vars($instance)) as $property) {
            $value = $settings[$property] ?? null;
            if ($value === null) {
                continue;
            }

            $mappedEnum = self::PROPERTY_ENUM_MAPPING[$property] ?? null;
            if ($mappedEnum && !in_array($value, $mappedEnum, true)) {
                throw new InvalidConfigurationException("Value '{$value}' is not valid for the {$property} option");
            }

            $instance->{$property} = $value;
        }

        return $instance;
    }

    public function getChainStrategy(): string
    {
        return $this->chainStrategy;
    }

    public function getRuleStrategy(): string
    {
        return $this->ruleStrategy;
    }

    public function getRuleMatchAgainst(): string
    {
        return $this->ruleMatchAgainst;
    }

    /**
     * @return string[]
     */
    public function getChainWhitelists(): array
    {
        return $this->chainWhitelists;
    }

    /**
     * @return string[]
     */
    public function getRuleWhitelists(): array
    {
        return $this->ruleWhitelists;
    }
}
