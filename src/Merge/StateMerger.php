<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge;

use Ds\Set;
use OneOfZero\IptMerge\Exception\NotImplementedException;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Merge\Configuration\Unmanaged;
use OneOfZero\IptMerge\Merge\Strategy\ChainKeep;
use OneOfZero\IptMerge\Merge\Strategy\ChainPurge;
use OneOfZero\IptMerge\Merge\Strategy\ChainWhitelist;
use OneOfZero\IptMerge\Merge\Strategy\RuleKeep;
use OneOfZero\IptMerge\Merge\Strategy\RulePurge;
use OneOfZero\IptMerge\Merge\Strategy\RuleWhitelist;
use OneOfZero\IptMerge\Merge\Strategy\StrategyInterface;
use OneOfZero\IptMerge\Netfilter\Structure\Chain;
use OneOfZero\IptMerge\Netfilter\Structure\State;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

class StateMerger
{
    private MergeBehaviour $mergeBehaviour;

    /**
     * @var StrategyInterface[]
     * @psalm-var array<string, StrategyInterface>
     */
    private array $chainStrategyMapping;

    /**
     * @var StrategyInterface[]
     * @psalm-var array<string, StrategyInterface>
     */
    private array $ruleStrategyMapping;

    public function __construct(MergeBehaviour $mergeBehaviour)
    {
        $this->mergeBehaviour = $mergeBehaviour;

        $this->chainStrategyMapping = [
            Unmanaged::STRATEGY_KEEP      => new ChainKeep(),
            Unmanaged::STRATEGY_PURGE     => new ChainPurge(),
            Unmanaged::STRATEGY_WHITELIST => new ChainWhitelist(),
        ];

        $this->ruleStrategyMapping = [
            Unmanaged::STRATEGY_KEEP      => new RuleKeep(),
            Unmanaged::STRATEGY_PURGE     => new RulePurge(),
            Unmanaged::STRATEGY_WHITELIST => new RuleWhitelist(),
        ];
    }

    public function merge(State $base, State $supplicant): State
    {
        $baseTables       = $base->getTablesIndexedByName();
        $supplicantTables = $supplicant->getTablesIndexedByName();

        $mergedTables = [];

        foreach ($baseTables as $tableName => $baseTable) {
            $supplicantTable = $supplicantTables[$tableName] ?? null;

            if ($supplicantTable === null) {
                $supplicantTable = new Table($tableName, new Set(), new Set());
            }

            $supplicantTable = $this->transplantCounters($baseTable, $supplicantTable);
            $supplicantTable = $this->mergeChains($baseTable, $supplicantTable);
            $supplicantTable = $this->mergeRules($baseTable, $supplicantTable);

            $mergedTables[] = $supplicantTable;
        }

        return new State($mergedTables);
    }

    private function mergeChains(Table $baseTable, Table $supplicantTable): Table
    {
        $strategy = $this->chainStrategyMapping[$this->mergeBehaviour->getUnmanaged()->getChainStrategy()] ?? null;

        if ($strategy === null) {
            throw new NotImplementedException("Chain merge strategy '{$strategy}' is not implemented or mapped");
        }

        return $strategy->process($baseTable, $supplicantTable, $this->mergeBehaviour);
    }

    private function mergeRules(Table $baseTable, Table $supplicantTable): Table
    {
        $strategy = $this->ruleStrategyMapping[$this->mergeBehaviour->getUnmanaged()->getRuleStrategy()] ?? null;

        if ($strategy === null) {
            throw new NotImplementedException("Rule merge strategy '{$strategy}' is not implemented or mapped");
        }

        return $strategy->process($baseTable, $supplicantTable, $this->mergeBehaviour);
    }

    private function transplantCounters(Table $base, Table $supplicant): Table
    {
        $baseChains       = $base->getChainsIndexedByName();
        $supplicantChains = $supplicant->getChainsIndexedByName();

        $supplicantChains->apply(
            static function (string $name, Chain $supplicantChain) use ($baseChains): Chain {
                /** @var Chain $baseChain */
                $baseChain = $baseChains->get($name, null);
                return $baseChain
                    ? $supplicantChain->withCounters($baseChain->getCounters())
                    : $supplicantChain;
            }
        );

        return new Table(
            $supplicant->getName(),
            new Set($supplicantChains->values()),
            $supplicant->getRules()
        );
    }
}
