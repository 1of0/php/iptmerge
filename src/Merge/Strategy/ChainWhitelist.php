<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Strategy;

use Ds\Set;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

class ChainWhitelist extends AbstractStrategy implements StrategyInterface
{
    public function process(Table $base, Table $supplicant, MergeBehaviour $mergeBehaviour): Table
    {
        $extraneousChains = $base->getChains()->diff($supplicant->getChains());

        $whitelisted = $this->getWhitelistedChains($extraneousChains->toArray(), $mergeBehaviour);

        $baseChainsToRemove = $extraneousChains->diff($whitelisted);

        $chainDeletions = $this->getChainDeletionRules($baseChainsToRemove->toArray());

        return new Table(
            $supplicant->getName(),
            $whitelisted->union($supplicant->getChains()),
            (new Set($chainDeletions))->union($supplicant->getRules()),
        );
    }
}
