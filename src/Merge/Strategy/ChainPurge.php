<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Strategy;

use Ds\Set;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

class ChainPurge extends AbstractStrategy implements StrategyInterface
{
    public function process(Table $base, Table $supplicant, MergeBehaviour $mergeBehaviour): Table
    {
        $extraneousChains = $base->getChains()->diff($supplicant->getChains());

        $chainDeletions = $this->getChainDeletionRules($extraneousChains->toArray());

        return new Table(
            $supplicant->getName(),
            $supplicant->getChains(),
            (new Set($chainDeletions))->union($supplicant->getRules()),
        );
    }
}
