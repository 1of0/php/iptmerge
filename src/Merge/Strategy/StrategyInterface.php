<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Strategy;

use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

interface StrategyInterface
{
    public function process(Table $base, Table $supplicant, MergeBehaviour $mergeBehaviour): Table;
}
