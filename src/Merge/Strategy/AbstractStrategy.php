<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Strategy;

use Ds\Set;
use OneOfZero\IptMerge\Exception\NotImplementedException;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Merge\Configuration\Unmanaged;
use OneOfZero\IptMerge\Netfilter\Structure\Chain;
use OneOfZero\IptMerge\Netfilter\Structure\Rule;

abstract class AbstractStrategy
{
    /**
     * @param Chain[]        $chains
     * @param MergeBehaviour $mergeBehaviour
     * @return Set
     */
    protected function getManagedChains(array $chains, MergeBehaviour $mergeBehaviour): Set
    {
        $filtered = array_filter(
            $chains,
            static function (Chain $chain) use ($mergeBehaviour): bool {
                return preg_match("/{$mergeBehaviour->getManaged()->getChainPattern()}/", $chain->getName()) === 1;
            }
        );
        return new Set($filtered);
    }

    /**
     * @param Rule[]         $rules
     * @param MergeBehaviour $mergeBehaviour
     * @return Set
     */
    protected function getManagedRules(array $rules, MergeBehaviour $mergeBehaviour): Set
    {
        $filtered = array_filter(
            $rules,
            static function (Rule $rule) use ($mergeBehaviour): bool {
                $commentOption = $rule->getFirstOptionByName('comment');

                if ($commentOption === null) {
                    return false;
                }

                $comment = $commentOption->getValue(0);
                return preg_match("/{$mergeBehaviour->getManaged()->getRulePattern()}/", $comment) === 1;
            }
        );
        return new Set($filtered);
    }

    /**
     * @param Chain[]        $chains
     * @param MergeBehaviour $mergeBehaviour
     * @return Set
     */
    protected function getWhitelistedChains(array $chains, MergeBehaviour $mergeBehaviour): Set
    {
        $filtered = array_filter(
            $chains,
            static function (Chain $chain) use ($mergeBehaviour): bool {
                foreach ($mergeBehaviour->getUnmanaged()->getChainWhitelists() as $pattern) {
                    if (preg_match("/{$pattern}/", $chain->getName())) {
                        return true;
                    }
                }
                return false;
            }
        );
        return new Set($filtered);
    }

    /**
     * @param Rule[]         $rules
     * @param MergeBehaviour $mergeBehaviour
     * @return Set
     */
    protected function getWhitelistedRules(array $rules, MergeBehaviour $mergeBehaviour): Set
    {
        $filtered = array_filter(
            $rules,
            static function (Rule $rule) use ($mergeBehaviour): bool {
                $matchTarget = $mergeBehaviour->getUnmanaged()->getRuleMatchAgainst();

                switch ($matchTarget) {
                    case Unmanaged::MATCH_FULL:
                        $matchAgainst = (string)$rule;
                        break;
                    case Unmanaged::MATCH_COMMENT:
                        $commentOption = $rule->getFirstOptionByName('comment');
                        if ($commentOption === null) {
                            return false;
                        }
                        $matchAgainst = $commentOption->getValue(0);
                        break;
                    default:
                        throw new NotImplementedException("Match target {$matchTarget} is not implemented");
                }

                foreach ($mergeBehaviour->getUnmanaged()->getRuleWhitelists() as $pattern) {
                    if (preg_match("/{$pattern}/", $matchAgainst)) {
                        return true;
                    }
                }
                return false;
            }
        );
        return new Set($filtered);
    }

    /**
     * @param Chain[] $chains
     * @return Rule[]
     */
    protected function getChainDeletionRules(array $chains): array
    {
        return array_map(
            static function (Chain $chain): Rule {
                return new Rule('delete-chain', $chain->getName());
            },
            $chains
        );
    }

    /**
     * @param Rule[] $rules
     * @return Rule[]
     */
    protected function getRuleDeletionRules(array $rules): array
    {
        return array_map(
            static function (Rule $rule): Rule {
                return $rule->withAction('delete');
            },
            $rules
        );
    }
}
