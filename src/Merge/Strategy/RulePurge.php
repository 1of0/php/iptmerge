<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Strategy;

use Ds\Set;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

/**
 * Rule strategy that purges unmanaged rules.
 *
 * We just purge all rules from the base table that don't exist in the supplicant table, but do add the tables in the
 * supplicant table that don't exist in the base table.
 */
class RulePurge extends AbstractStrategy implements StrategyInterface
{
    public function process(Table $base, Table $supplicant, MergeBehaviour $mergeBehaviour): Table
    {
        $extraneousRules = $base->getRules()->diff($supplicant->getRules());

        $ruleDeletions = $this->getRuleDeletionRules($extraneousRules->toArray());

        return new Table(
            $supplicant->getName(),
            $supplicant->getChains(),
            (new Set($ruleDeletions))->union($supplicant->getRules()->diff($base->getRules())),
        );
    }
}
