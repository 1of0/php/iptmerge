<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Strategy;

use Ds\Set;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

/**
 * Rule strategy that only keeps whitelisted unmanaged rules.
 *
 * We figure out which of the rules in the base table (that don't exist in the supplicant table) are whitelisted. The
 * whitelisted rules are not touched, and the other rules in the base table (that don't exist in the supplicant table)
 * are deleted. Rules in the supplicant table that don't exist in the base table are added.
 */
class RuleWhitelist extends AbstractStrategy implements StrategyInterface
{
    public function process(Table $base, Table $supplicant, MergeBehaviour $mergeBehaviour): Table
    {
        $extraneousRules = $base->getRules()->diff($supplicant->getRules());

        $whitelisted = $this->getWhitelistedRules($extraneousRules->toArray(), $mergeBehaviour);

        $baseRulesToRemove = $extraneousRules->diff($whitelisted);

        $ruleDeletions = $this->getRuleDeletionRules($baseRulesToRemove->toArray());

        $supplicantAdditions = $supplicant->getRules()->diff($base->getRules());

        return new Table(
            $supplicant->getName(),
            $supplicant->getChains(),
            (new Set($ruleDeletions))->union($supplicantAdditions),
        );
    }
}
