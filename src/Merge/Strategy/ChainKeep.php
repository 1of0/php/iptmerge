<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Strategy;

use Ds\Set;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

class ChainKeep extends AbstractStrategy implements StrategyInterface
{
    public function process(Table $base, Table $supplicant, MergeBehaviour $mergeBehaviour): Table
    {
        $managedChains = $this->getManagedChains($base->getChains()->toArray(), $mergeBehaviour);

        $extraneousManagedChains = $managedChains->diff($supplicant->getChains());

        $baseChainsWithoutExtraneousChains = $base->getChains()->diff($extraneousManagedChains);

        $chainDeletions = $this->getChainDeletionRules($extraneousManagedChains->toArray());

        return new Table(
            $supplicant->getName(),
            $baseChainsWithoutExtraneousChains->union($supplicant->getChains()),
            (new Set($chainDeletions))->union($supplicant->getRules()),
        );
    }
}
