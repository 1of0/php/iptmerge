<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Merge\Strategy;

use Ds\Set;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Netfilter\Structure\Table;

/**
 * Rule strategy that keeps unmanaged rules.
 *
 * Basically we just want to add new rules appearing in the supplicant table and remove managed rules in the base
 * table that don't exist in the supplicant table anymore.
 */
class RuleKeep extends AbstractStrategy implements StrategyInterface
{
    public function process(Table $base, Table $supplicant, MergeBehaviour $mergeBehaviour): Table
    {
        $managedRules = $this->getManagedRules($base->getRules()->toArray(), $mergeBehaviour);

        $extraneousManagedRules = $managedRules->diff($supplicant->getRules());

        $baseRulesWithoutExtraneousRules = $base->getRules()->diff($extraneousManagedRules);

        $ruleDeletions = $this->getRuleDeletionRules($extraneousManagedRules->toArray());

        return new Table(
            $supplicant->getName(),
            $supplicant->getChains(),
            (new Set($ruleDeletions))->union($supplicant->getRules()->diff($baseRulesWithoutExtraneousRules)),
        );
    }
}
