<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter;

use Ds\Set;
use OneOfZero\IptMerge\Exception\ParseException;
use OneOfZero\IptMerge\Netfilter\Netfilter;
use OneOfZero\IptMerge\Netfilter\Parse\StateParser;
use OneOfZero\IptMerge\Netfilter\Structure\Chain;
use OneOfZero\IptMerge\Netfilter\Structure\State;
use OneOfZero\IptMerge\Netfilter\Structure\Table;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class NetfilterTest extends TestCase
{
    private State $expectedState;

    private string $stateFile;

    private Netfilter $instance;

    public function setUp(): void
    {
        $stateContent = "*filter\n:INPUT ACCEPT [0:0]\nCOMMIT\n";

        $this->expectedState = new State([
            new Table('filter', new Set([new Chain('INPUT', 'ACCEPT', '[0:0]')]), new Set())
        ]);

        $this->instance = new Netfilter();

        $this->stateFile = tempnam(sys_get_temp_dir(), 'iptmerge-test');

        if (file_put_contents($this->stateFile, $stateContent) === false) {
            throw new RuntimeException("Failed setting up state file '{$this->stateFile}' for test");
        }
    }

    public function testParseStateFromFile(): void
    {
        $this->assertEquals($this->expectedState, $this->instance->parseStateFromFile($this->stateFile));
    }

    public function testUnreadable(): void
    {
        if (function_exists('posix_getuid') && posix_getuid() === 0) {
            $this->markTestSkipped("Can't do testUnreadable as root");
        }
        chmod($this->stateFile, '0000');
        $this->expectException(ParseException::class);
        $this->instance->parseStateFromFile($this->stateFile);
    }

    public function testDirectory(): void
    {
        unlink($this->stateFile);
        mkdir($this->stateFile);
        $this->expectException(ParseException::class);
        $this->instance->parseStateFromFile($this->stateFile);
    }

    public function testMissing(): void
    {
        unlink($this->stateFile);
        $this->expectException(ParseException::class);
        $this->instance->parseStateFromFile($this->stateFile);
    }

    public function tearDown(): void
    {
        if (file_exists($this->stateFile) && is_file($this->stateFile)) {
            unlink($this->stateFile);
        }
        if (file_exists($this->stateFile) && is_dir($this->stateFile)) {
            rmdir($this->stateFile);
        }
    }
}
