<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Parse;

use OneOfZero\IptMerge\Netfilter\Parse\StringHelper;
use PHPUnit\Framework\TestCase;

class StringHelperTest extends TestCase
{
    /**
     * @param string $value
     *
     * @dataProvider unescapedValueProvider
     */
    public function testEscapeAndUnescape(string $value): void
    {
        $this->assertEquals($value, StringHelper::unescape(StringHelper::escape($value)));
    }

    /**
     * @param string      $value
     * @param string|null $expectedOutput
     *
     * @dataProvider escapedValueProvider
     */
    public function testUnescapeAndEscape(string $value, ?string $expectedOutput = null): void
    {
        if ($expectedOutput === null) {
            $expectedOutput = $value;
        }
        $this->assertEquals($expectedOutput, StringHelper::escape(StringHelper::unescape($value)));
    }

    public function unescapedValueProvider(): array
    {
        return [
            ['Foo bar baz'],
            ['Foo "bar" baz'],
            ["Foo 'bar' baz"],
            ['Foo \\bar baz'],
            ['Foo bar baz\\\\'],
            ['Foo bar baz\\'],
            ['Foo bar baz\\\''],
            ['Foo bar baz\\\\\''],
            ['"Foo bar baz"'],
        ];
    }

    public function escapedValueProvider(): array
    {
        return [
            ["'Foo bar baz'"],
            ["'Foo \\'bar\\' baz'"],
            ["'Foo \\\"bar\\\" baz'"],
            ["'Foo\\\\bar\\\\baz'"],
            ['"Foo bar baz"', "'Foo bar baz'"],
            ['Foo bar baz', "'Foo bar baz'"],
        ];
    }
}
