<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Parse;

use OneOfZero\IptMerge\Exception\InvalidDefinitionException;
use OneOfZero\IptMerge\Exception\InvalidActionException;
use OneOfZero\IptMerge\Exception\UnexpectedTokenException;
use OneOfZero\IptMerge\Netfilter\Parse\RuleParser;
use OneOfZero\IptMerge\Netfilter\Parse\StringHelper;
use OneOfZero\IptMerge\Netfilter\Structure\Rule;
use OneOfZero\IptMerge\Netfilter\Structure\RuleOption;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    /**
     * @param string $definition
     * @param Rule[] $expectedRules
     *
     * @dataProvider validDefinitionsProvider
     */
    public function testParseRuleValidInput(string $definition, array $expectedRules): void
    {
        $parser = new RuleParser();

        $actualRules = $parser->parse($definition);
        $this->assertEquals($expectedRules, $actualRules);

        if (count($actualRules) === 1) {
            $escapableCharacters = array_keys(StringHelper::ESCAPE_MAP);

            // Strip all quotes from definition and string conversion of the parsed Rule and compare the results.
            // This will miss escaping issues, but those should be covered by other tests.
            $fuzzyExpected = str_replace($escapableCharacters, '', $definition);
            $fuzzyActual   = str_replace($escapableCharacters, '', $actualRules[0]->__toString());

            $this->assertEquals($fuzzyExpected, $fuzzyActual);
        }
    }

    /**
     * @param string $definition
     * @param string $expectedExceptionClass
     *
     * @dataProvider invalidDefinitionsProvider
     */
    public function testParseRuleInvalidInput(string $definition, string $expectedExceptionClass): void
    {
        $parser = new RuleParser();

        $this->expectException($expectedExceptionClass);
        $parser->parse($definition);
    }

    public function validDefinitionsProvider(): array
    {
        return [
            [
                '-A INPUT -p tcp --dport 1234 -j ACCEPT',
                [
                    new Rule(
                        'A',
                        'INPUT',
                        [
                            new RuleOption('p', ['tcp']),
                            new RuleOption('dport', ['1234']),
                            new RuleOption('j', ['ACCEPT']),
                        ]
                    ),
                ],
            ],
            [
                '-I INPUT -p tcp --dport 1234 -j ACCEPT',
                [
                    new Rule(
                        'I',
                        'INPUT',
                        [
                            new RuleOption('p', ['tcp']),
                            new RuleOption('dport', ['1234']),
                            new RuleOption('j', ['ACCEPT']),
                        ]
                    ),
                ],
            ],
            [
                '-I INPUT -p tcp ! --dport 1234 -j ACCEPT',
                [
                    new Rule(
                        'I',
                        'INPUT',
                        [
                            new RuleOption('p', ['tcp']),
                            new RuleOption('dport', ['1234'], true),
                            new RuleOption('j', ['ACCEPT']),
                        ]
                    ),
                ],
            ],
            [
                '-I INPUT -m multiport --dports 123,456,789 -m comment --comment "foo" -j ACCEPT',
                [
                    new Rule(
                        'I',
                        'INPUT',
                        [
                            new RuleOption('m', ['multiport']),
                            new RuleOption('dports', ['123,456,789']),
                            new RuleOption('m', ['comment']),
                            new RuleOption('comment', ['foo']),
                            new RuleOption('j', ['ACCEPT']),
                        ]
                    ),
                ],
            ],
            [
                '-X CUSTOM-CHAIN',
                [new Rule('X', 'CUSTOM-CHAIN', [])],
            ],
            [
                '-A INPUT -s 1.1.1.1/32,2.2.2.2/32,3.3.3.3/32 -j ACCEPT',
                [
                    new Rule('A', 'INPUT', [new RuleOption('s', ['1.1.1.1/32']), new RuleOption('j', ['ACCEPT'])]),
                    new Rule('A', 'INPUT', [new RuleOption('s', ['2.2.2.2/32']), new RuleOption('j', ['ACCEPT'])]),
                    new Rule('A', 'INPUT', [new RuleOption('s', ['3.3.3.3/32']), new RuleOption('j', ['ACCEPT'])]),
                ],
            ],
            [
                '-A INPUT -s 1.1.1.1/32 -j ACCEPT',
                [
                    new Rule('A', 'INPUT', [new RuleOption('s', ['1.1.1.1/32']), new RuleOption('j', ['ACCEPT'])]),
                ],
            ],
        ];
    }

    public function invalidDefinitionsProvider(): array
    {
        return [
            ['foo -A INPUT -j ACCEPT', UnexpectedTokenException::class],
            ['-p tcp --dport 1234 -j ACCEPT', InvalidActionException::class],
            ['-R INPUT 1 -j ACCEPT', InvalidActionException::class],
            ['-A -j ACCEPT', InvalidDefinitionException::class],
            ['', InvalidActionException::class],
            ['-A INPUT -s foo -s bar', InvalidDefinitionException::class],
            ['-A INPUT -s foo bar', InvalidDefinitionException::class],
            ['-A INPUT -A INPUT', InvalidActionException::class],
        ];
    }
}
