<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Parse;

use Ds\Set;
use OneOfZero\IptMerge\Exception\InvalidDefinitionException;
use OneOfZero\IptMerge\Netfilter\Parse\StateParser;
use OneOfZero\IptMerge\Netfilter\Structure\Chain;
use OneOfZero\IptMerge\Netfilter\Structure\Rule;
use OneOfZero\IptMerge\Netfilter\Structure\RuleOption;
use OneOfZero\IptMerge\Netfilter\Structure\State;
use OneOfZero\IptMerge\Netfilter\Structure\Table;
use PHPUnit\Framework\TestCase;

class StateParserTest extends TestCase
{
    /**
     * @param string $stateDefinition
     * @param State  $expectedState
     *
     * @dataProvider validParseStateProvider
     */
    public function testValidParseState(string $stateDefinition, State $expectedState): void
    {
        $parser = new StateParser();
        $this->assertEquals($expectedState, $parser->parse($stateDefinition));
    }

    /**
     * @param string $stateDefinition
     * @param string $expectedExceptionClass
     *
     * @dataProvider invalidParseStateProvider
     */
    public function testInvalidParseState(string $stateDefinition, string $expectedExceptionClass): void
    {
        $parser = new StateParser();
        $this->expectException($expectedExceptionClass);
        $parser->parse($stateDefinition);
    }

    public function validParseStateProvider(): array
    {
        return [
            [
                "#comment\n*foo\nCOMMIT\n\n*bar\nCOMMIT\n",
                new State([
                    new Table('foo', new Set(), new Set()),
                    new Table('bar', new Set(), new Set()),
                ]),
            ],
            [
                "#comment\n*foo\n:INPUT ACCEPT [0:0]\nCOMMIT\n",
                new State([
                    new Table('foo', new Set([new Chain('INPUT', 'ACCEPT', '[0:0]')]), new Set()),
                ]),
            ],
            [
                "#comment\n*foo\n-A INPUT -s 127.0.0.1 -j ACCEPT\nCOMMIT\n",
                new State([
                    new Table(
                        'foo',
                        new Set(),
                        new Set([
                            new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1']), new RuleOption('j', ['ACCEPT'])
                        ])
                    ])),
                ]),
            ],
            [
                "#comment\n*foo\n:INPUT ACCEPT [0:0]\n-A INPUT -s 127.0.0.1 -j ACCEPT\nCOMMIT\n",
                new State([
                    new Table(
                        'foo',
                        new Set([
                            new Chain('INPUT', 'ACCEPT', '[0:0]')
                        ]),
                        new Set([
                            new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1']), new RuleOption('j', ['ACCEPT'])
                        ])
                    ])),
                ]),
            ],
            [
                '',
                new State([]),
            ],
            [
                '#comment',
                new State([]),
            ],
        ];
    }

    public function invalidParseStateProvider(): array
    {
        return [
            [":INPUT\n*foo\nCOMMIT\n", InvalidDefinitionException::class],
        ];
    }
}
