<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Parse;

use InvalidArgumentException;
use OneOfZero\IptMerge\Exception\UnclosedStringException;
use OneOfZero\IptMerge\Netfilter\Parse\RuleTokenizer;
use PHPUnit\Framework\TestCase;

class RuleTokenizerTest extends TestCase
{
    /**
     * @param string $definition
     * @param array  $expectedTokens
     * @param array  $expectedTokenGroups
     *
     * @dataProvider validDefinitionsProvider
     */
    public function testTokenizerValidInput(string $definition, array $expectedTokens, array $expectedTokenGroups): void
    {
        $tokenizer = new RuleTokenizer();

        $actualTokens = $tokenizer->tokenize($definition);
        $this->assertSame($expectedTokens, $actualTokens);

        $actualTokenGroups = $tokenizer->groupTokens($actualTokens);
        $this->assertSame($expectedTokenGroups, $actualTokenGroups);
    }

    /**
     * @param string $definition
     * @param string $expectedExceptionClass
     *
     * @dataProvider invalidDefinitionsProvider
     */
    public function testTokenizerInvalidInput(string $definition, string $expectedExceptionClass): void
    {
        $tokenizer = new RuleTokenizer();

        $this->expectException($expectedExceptionClass);
        $tokenizer->tokenize($definition);
    }

    public function validDefinitionsProvider(): array
    {
        return [
            [
                '-A INPUT -p tcp --dport 1234 -j ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                "-A INPUT -p\ntcp        --dport 1234    -j\tACCEPT         ",
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT -p=tcp --dport 1234 -j ACCEPT',
                ['-A', 'INPUT', '-p', '=tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', '=tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT -p tcp --dport=1234 -j ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT -p tcp ! --dport 1234 -j ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '!', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['!', '--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-AINPUT -p tcp ! --dport 1234 -j ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '!', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['!', '--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT -p"tcp" --dport 1234 -j ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT -p tcp --dport "1234" -j ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT -p tcp --dport="1234" -j ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT -ptcp --dport 1234 -j ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT "-p" "tcp" "--dport" "1234" "-j" ACCEPT',
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                "-A INPUT '-p' 'tcp' '--dport' '1234' '-j' ACCEPT",
                ['-A', 'INPUT', '-p', 'tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p', 'tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT "-p tcp" --dport 1234 -j ACCEPT',
                ['-A', 'INPUT', '-p tcp', '--dport', '1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p tcp'], ['--dport', '1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT "-p tcp" "--dport=1234" -j ACCEPT',
                ['-A', 'INPUT', '-p tcp', '--dport=1234', '-j', 'ACCEPT'],
                [['-A', 'INPUT'], ['-p tcp'], ['--dport=1234'], ['-j', 'ACCEPT']],
            ],
            [
                '-A INPUT -j ACCEPT -m comment --comment "Foo \"Bar\" Baz"',
                ['-A', 'INPUT', '-j', 'ACCEPT', '-m', 'comment', '--comment', 'Foo "Bar" Baz'],
                [['-A', 'INPUT'], ['-j', 'ACCEPT'], ['-m', 'comment'], ['--comment', 'Foo "Bar" Baz']],
            ],
            [
                '-A INPUT -j ACCEPT -m comment --comment "Foo \\\\\"Bar\" Baz"',
                ['-A', 'INPUT', '-j', 'ACCEPT', '-m', 'comment', '--comment', 'Foo \\"Bar" Baz'],
                [['-A', 'INPUT'], ['-j', 'ACCEPT'], ['-m', 'comment'], ['--comment', 'Foo \\"Bar" Baz']],
            ],
            [
                '""',
                [''],
                [['']],
            ],
            [
                '=',
                ['='],
                [['=']],
            ],
            [
                '',
                [],
                [],
            ],
        ];
    }

    public function invalidDefinitionsProvider(): array
    {
        return [
            ['"', UnclosedStringException::class],
            ['-A INPUT -j ACCEPT -m comment --comment "Foo', UnclosedStringException::class],
            ['-A INPUT -j ACCEPT -m comment --comment "Foo\"', UnclosedStringException::class],
        ];
    }
}
