<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Structure;

use OneOfZero\IptMerge\Netfilter\Structure\RuleOption;
use OneOfZero\IptMerge\Netfilter\Structure\RuleOptionCollection;
use PHPUnit\Framework\TestCase;

class RuleOptionCollectionTest extends TestCase
{
    /**
     * @param RuleOptionCollection $collection
     * @param string               $optionName
     * @param array                $expectedOptions
     *
     * @dataProvider getOptionsByNameProvider
     */
    public function testGetOptionsByName(
        RuleOptionCollection $collection,
        string $optionName,
        array $expectedOptions
    ): void {
        $this->assertEquals($expectedOptions, $collection->getOptionsByName($optionName));
    }

    /**
     * @param RuleOptionCollection $collection
     * @param array                $optionNames
     * @param array                $expectedOptions
     *
     * @dataProvider getOptionsByAnyNameProvider
     */
    public function testGetOptionsByAnyName(
        RuleOptionCollection $collection,
        array $optionNames,
        array $expectedOptions
    ): void {
        $this->assertEquals($expectedOptions, $collection->getOptionsByAnyName($optionNames));
    }

    /**
     * @param RuleOptionCollection $collection
     * @param string               $optionName
     * @param array                $expectedOptions
     *
     * @dataProvider getOptionsByNameProvider
     */
    public function testGetFirstOptionByName(
        RuleOptionCollection $collection,
        string $optionName,
        array $expectedOptions
    ): void {
        $this->assertEquals($expectedOptions[0] ?? null, $collection->getFirstOptionByName($optionName));
    }

    /**
     * @param RuleOptionCollection $collection
     * @param array                $optionNames
     * @param array                $expectedOptions
     *
     * @dataProvider getOptionsByAnyNameProvider
     */
    public function testGetFirstOptionByAnyName(
        RuleOptionCollection $collection,
        array $optionNames,
        array $expectedOptions
    ): void {
        $this->assertEquals($expectedOptions[0] ?? null, $collection->getFirstOptionByAnyName($optionNames));
    }

    /**
     * @param RuleOptionCollection $collection
     * @param RuleOption           $optionToRemove
     * @param RuleOptionCollection $expectedCollection
     *
     * @dataProvider withoutOptionProvider
     */
    public function testWithoutOption(
        RuleOptionCollection $collection,
        RuleOption $optionToRemove,
        RuleOptionCollection $expectedCollection
    ): void {
        $this->assertEquals($expectedCollection, $collection->withoutOption($optionToRemove));
    }

    public function getOptionsByNameProvider(): array
    {
        return [
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                'a',
                [new RuleOption('a'), new RuleOption('a')],
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                'b',
                [new RuleOption('b')],
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                'c',
                [],
            ],
        ];
    }

    public function getOptionsByAnyNameProvider(): array
    {
        return [
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                ['a', 'b'],
                [new RuleOption('a'), new RuleOption('b'), new RuleOption('a')],
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                ['a'],
                [new RuleOption('a'), new RuleOption('a')],
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                ['b'],
                [new RuleOption('b')],
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                ['b', 'c'],
                [new RuleOption('b')],
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                ['c'],
                [],
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                [],
                [],
            ],
        ];
    }

    public function withoutOptionProvider(): array
    {
        return [
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                new RuleOption('b'),
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('a')]),
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                new RuleOption('a'),
                new RuleOptionCollection([new RuleOption('b')]),
            ],
            [
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
                new RuleOption('c'),
                new RuleOptionCollection([new RuleOption('a'), new RuleOption('b'), new RuleOption('a')]),
            ],
        ];
    }
}
