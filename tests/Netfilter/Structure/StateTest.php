<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Structure;

use Ds\Set;
use OneOfZero\IptMerge\Netfilter\Structure\State;
use OneOfZero\IptMerge\Netfilter\Structure\Table;
use PHPUnit\Framework\TestCase;

class StateTest extends TestCase
{
    public function testGetTablesIndexedByName(): void
    {
        $tables = [
            'foo' => new Table('foo', new Set(), new Set()),
            'bar' => new Table('bar', new Set(), new Set()),
        ];
        $state = new State(array_values($tables));
        $this->assertEquals($tables, $state->getTablesIndexedByName());
        $this->assertEquals("*foo\nCOMMIT\n*bar\nCOMMIT\n", (string)$state);
    }
}
