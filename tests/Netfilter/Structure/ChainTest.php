<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Structure;

use OneOfZero\IptMerge\Netfilter\Structure\Chain;
use PHPUnit\Framework\TestCase;

class ChainTest extends TestCase
{
    /**
     * @param Chain  $chain
     * @param string $expectedName
     * @param string $expectedPolicy
     * @param string $expectedCounters
     *
     * @dataProvider getterProvider
     */
    public function testGetters(
        Chain $chain,
        string $expectedName,
        string $expectedPolicy,
        string $expectedCounters
    ): void {
        $this->assertEquals($expectedName, $chain->getName());
        $this->assertEquals($expectedPolicy, $chain->getPolicy());
        $this->assertEquals($expectedCounters, $chain->getCounters());

        $createdChain = new Chain($expectedName, $expectedPolicy, $expectedCounters);
        $this->assertEquals($expectedName, $createdChain->getName());
        $this->assertEquals($expectedPolicy, $createdChain->getPolicy());
        $this->assertEquals($expectedCounters, $createdChain->getCounters());
    }

    public function testWithChain(): void
    {
        $chain = new Chain('foo', 'ACCEPT', '[0:0]');

        $this->assertEquals('[1:1]', $chain->withCounters('[1:1]')->getCounters());
    }

    /**
     * @param Chain $a
     * @param Chain $b
     * @param bool  $expectEqual
     *
     * @dataProvider comparisonProvider
     */
    public function testComparison(Chain $a, Chain $b, bool $expectEqual): void
    {
        if ($expectEqual) {
            $this->assertTrue(
                $a->equals($b),
                "Chains do not match, while they should.\n\nChain A: {$a}\nChain B: {$b}"
            );
            $this->assertEquals(
                $a->hash(),
                $b->hash(),
            );
        } else {
            $this->assertFalse(
                $a->equals($b),
                "Chains match, while they shouldn't.\n\nChain A: {$a}\nChain B: {$b}"
            );
        }
    }

    public function getterProvider(): array
    {
        return [
            [new Chain('INPUT', 'ACCEPT', '[0:0]'), 'INPUT', 'ACCEPT', '[0:0]'],
            [new Chain('INPUT', '-', '[0:0]'), 'INPUT', '-', '[0:0]'],
        ];
    }

    public function comparisonProvider(): array
    {
        return [
            [new Chain('INPUT', 'ACCEPT', '[0:0]'), new Chain('INPUT', 'ACCEPT', '[0:0]'), true],
            [new Chain('INPUT', 'ACCEPT', '[0:0]'), new Chain('INPUT', 'DROP', '[0:0]'), true],
            [new Chain('INPUT', 'ACCEPT', '[0:0]'), new Chain('INPUT', 'ACCEPT', '[123:456]'), true],
            [new Chain('INPUT', 'ACCEPT', '[0:0]'), new Chain('OUTPUT', 'ACCEPT', '[0:0]'), false],
            [new Chain('INPUT', 'ACCEPT', '[0:0]'), new Chain('OUTPUT', 'DROP', '[0:0]'), false],
            [new Chain('INPUT', 'ACCEPT', '[0:0]'), new Chain('OUTPUT', 'ACCEPT', '[123:456]'), false],
        ];
    }
}
