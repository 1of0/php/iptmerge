<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Structure;

use Ds\Set;
use OneOfZero\IptMerge\Netfilter\Structure\Chain;
use OneOfZero\IptMerge\Netfilter\Structure\Rule;
use OneOfZero\IptMerge\Netfilter\Structure\Table;
use PHPUnit\Framework\TestCase;

class TableTest extends TestCase
{
    /**
     * @param Table $table
     * @param string $expectedName
     * @param array $expectedChains
     * @param array $expectedRules
     * @param string $expectedToString
     *
     * @dataProvider getterProvider
     */
    public function testGetters(
        Table $table,
        string $expectedName,
        array $expectedChains,
        array $expectedRules,
        string $expectedToString
    ): void
    {
        $this->assertEquals($expectedName, $table->getName());
        $this->assertEquals($expectedChains, $table->getChains()->toArray());
        $this->assertEquals($expectedRules, $table->getRules()->toArray());

        $expectedDebugInfo = [
            'name'   => $expectedName,
            'chains' => $expectedChains,
            'rules'  => $expectedRules,
        ];
        $this->assertEquals($expectedDebugInfo, $table->__debugInfo());

        $newTable = new Table($expectedName, new Set($expectedChains), new Set($expectedRules));
        $this->assertEquals($expectedName, $newTable->getName());
        $this->assertEquals($expectedChains, $newTable->getChains()->toArray());
        $this->assertEquals($expectedRules, $newTable->getRules()->toArray());
        $this->assertEquals($expectedToString, (string)$newTable);
    }

    public function getterProvider(): array
    {
        return [
            [
                new Table(
                    'filter',
                    new Set([new Chain('INPUT', 'ACCEPT', '[0:0]')]),
                    new Set([new Rule('A', 'INPUT')])
                ),
                'filter',
                [new Chain('INPUT', 'ACCEPT', '[0:0]')],
                [new Rule('A', 'INPUT')],
                "*filter\n:INPUT ACCEPT [0:0]\n-A 'INPUT'\nCOMMIT\n",
            ],
            [
                new Table(
                    'filter',
                    new Set([new Chain('INPUT', 'ACCEPT', '[0:0]')]),
                    new Set()
                ),
                'filter',
                [new Chain('INPUT', 'ACCEPT', '[0:0]')],
                [],
                "*filter\n:INPUT ACCEPT [0:0]\nCOMMIT\n",
            ],
            [
                new Table(
                    'filter',
                    new Set(),
                    new Set([new Rule('A', 'INPUT')])
                ),
                'filter',
                [],
                [new Rule('A', 'INPUT')],
                "*filter\n-A 'INPUT'\nCOMMIT\n",
            ],
            [
                new Table(
                    'filter',
                    new Set(),
                    new Set()
                ),
                'filter',
                [],
                [],
                "*filter\nCOMMIT\n",
            ],
        ];
    }
}
