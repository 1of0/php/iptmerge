<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Structure;

use OneOfZero\IptMerge\Netfilter\Structure\Rule;
use OneOfZero\IptMerge\Netfilter\Structure\RuleOption;
use PHPUnit\Framework\TestCase;

class RuleTest extends TestCase
{
    /**
     * @param Rule         $rule
     * @param string       $expectedAction
     * @param string       $expectedChain
     * @param RuleOption[] $expectedOptions
     * @param string       $expectedString
     *
     * @dataProvider getterProvider
     */
    public function testGetters(
        Rule $rule,
        string $expectedAction,
        string $expectedChain,
        array $expectedOptions,
        string $expectedString
    ): void {
        $this->assertEquals($expectedAction, $rule->getAction());
        $this->assertEquals($expectedChain, $rule->getChain());
        $this->assertEquals($expectedOptions, $rule->getOptions());
        $this->assertEquals($expectedString, $rule->__toString());
        $this->assertEquals($expectedString, (string)$rule);
    }

    /**
     * @param Rule $a
     * @param Rule $b
     * @param bool $expectEqual
     *
     * @dataProvider comparisonProvider
     */
    public function testComparison(Rule $a, Rule $b, bool $expectEqual): void
    {
        if ($expectEqual) {
            $this->assertTrue(
                $a->equals($b),
                "Rules do not match, while they should.\n\nRule A: {$a}\nRule B: {$b}"
            );
            $this->assertEquals(
                $a->hash(),
                $b->hash(),
            );
        } else {
            $this->assertFalse(
                $a->equals($b),
                "Rules match, while they shouldn't.\n\nRule A: {$a}\nRule B: {$b}"
            );
        }
    }

    /**
     * @param Rule       $rule
     * @param string     $optionName
     * @param RuleOption $replacement
     * @param string     $expectedString
     *
     * @dataProvider replacementProvider
     */
    public function testReplacedOption(
        Rule $rule,
        string $optionName,
        RuleOption $replacement,
        string $expectedString
    ): void {
        $this->assertEquals($expectedString, (string)$rule->withReplacedOption($optionName, $replacement));
    }

    /**
     * @param Rule   $rule
     * @param string $actionReplacement
     * @param Rule   $expectedRule
     *
     * @dataProvider actionReplacementProvider
     */
    public function testActionReplacement(Rule $rule, string $actionReplacement, Rule $expectedRule): void
    {
        $this->assertEquals($expectedRule, $rule->withAction($actionReplacement));
    }

    public function getterProvider(): array
    {
        return [
            [
                new Rule('A', 'INPUT', []),
                'A',
                'INPUT',
                [],
                "-A 'INPUT'",
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                'A',
                'INPUT',
                [new RuleOption('s', ['127.0.0.1'])],
                "-A 'INPUT' -s '127.0.0.1/32'",
            ],
            [
                new Rule('A', 'INPUT', ['foo' => new RuleOption('s', ['127.0.0.1'])]),
                'A',
                'INPUT',
                [new RuleOption('s', ['127.0.0.1'])],
                "-A 'INPUT' -s '127.0.0.1/32'",
            ],
        ];
    }

    public function comparisonProvider(): array
    {
        return [
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                true,
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.1'])]),
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                true,
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1/32'])]),
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                true,
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                new Rule('I', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                true,
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                new Rule('A', 'OUTPUT', [new RuleOption('s', ['127.0.0.1'])]),
                false,
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                new Rule('A', 'INPUT', [new RuleOption('d', ['127.0.0.1'])]),
                false,
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1'])]),
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1']), new RuleOption('d', ['127.0.0.1'])]),
                false,
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('d', ['127.0.0.1']), new RuleOption('s', ['127.0.0.1'])]),
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1']), new RuleOption('d', ['127.0.0.1'])]),
                true,
            ],
        ];
    }

    public function replacementProvider(): array
    {
        return [
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1/32'])]),
                's',
                new RuleOption('d', ['127.0.0.1/32']),
                "-A 'INPUT' -d '127.0.0.1/32'",
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1/32']), new RuleOption('s', ['127.0.0.1/32'])]),
                's',
                new RuleOption('d', ['127.0.0.1/32']),
                "-A 'INPUT' -d '127.0.0.1/32' -d '127.0.0.1/32'",
            ],
            [
                new Rule('A', 'INPUT', [new RuleOption('s', ['127.0.0.1/32'])]),
                'd',
                new RuleOption('d', ['127.0.0.1/32']),
                "-A 'INPUT' -s '127.0.0.1/32'",
            ],
        ];
    }

    public function actionReplacementProvider(): array
    {
        return [
            [
                new Rule('A', 'INPUT'),
                'delete',
                new Rule('delete', 'INPUT'),
            ],
        ];
    }
}
