<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Netfilter\Structure;

use OneOfZero\IptMerge\Netfilter\Structure\RuleOption;
use PHPUnit\Framework\TestCase;

class RuleOptionTest extends TestCase
{
    /**
     * @param RuleOption $a
     * @param RuleOption $b
     * @param bool       $expectEqual
     *
     * @dataProvider comparisonProvider
     */
    public function testComparison(RuleOption $a, RuleOption $b, bool $expectEqual): void
    {
        if ($expectEqual) {
            $this->assertTrue(
                $a->equals($b),
                "Chains do not match, while they should.\n\nChain A: {$a}\nChain B: {$b}"
            );
            $this->assertEquals(
                $a->hash(),
                $b->hash(),
            );
        } else {
            $this->assertFalse(
                $a->equals($b),
                "Chains match, while they shouldn't.\n\nChain A: {$a}\nChain B: {$b}"
            );
        }
    }

    public function comparisonProvider(): array
    {
        return [
            [new RuleOption('foo', ['bar', 'baz'], false), new RuleOption('foo', ['bar', 'baz'], false), true],
            [new RuleOption('foo', [], false), new RuleOption('foo', [], false), true],
            [new RuleOption('foo', ['bar', 'baz'], false), new RuleOption('foo', [], false), false],
            [new RuleOption('foo', ['bar', 'baz'], false), new RuleOption('bar', ['bar', 'baz'], false), false],
            [new RuleOption('foo', ['bar', 'baz'], false), new RuleOption('foo', ['bar', 'baz'], true), false],
            [new RuleOption('foo', ['bar', 'baz'], false), new RuleOption('foo', ['bar'], false), false],
        ];
    }
}
