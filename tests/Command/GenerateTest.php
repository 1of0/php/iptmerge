<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Command;

use OneOfZero\IptMerge\Command\Generate;
use OneOfZero\IptMerge\Netfilter\Netfilter;
use OneOfZero\IptMerge\Netfilter\Structure\State;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\BufferedOutput;

class GenerateTest extends TestCase
{
    public function testCommand(): void
    {
        $input = new ArgvInput([
            'iptmerge',
            __DIR__ . '/../Fixture/merge-behaviour.yml',
            __DIR__ . '/../Fixture/generated.state',
            __DIR__ . '/../Fixture/running.state',
        ]);
        $output = new BufferedOutput();

        $app = new Application();
        $app->add(new Generate());
        $app->setDefaultCommand('merge', true);
        $app->setAutoExit(false);
        $app->run($input, $output);

        $this->assertEquals(
            (string)$this->loadFixture('merged.whitelist.whitelist'),
            $output->fetch()
        );
    }

    public function testCommandStdin(): void
    {
        $input = new ArgvInput([
            'iptmerge',
            __DIR__ . '/../Fixture/merge-behaviour.yml',
            __DIR__ . '/../Fixture/generated.state',
        ]);
        $output = new BufferedOutput();

        $runningStateStream = null;

        try {
            $runningStateStream = fopen(__DIR__ . '/../Fixture/running.state', 'rb');
            $input->setStream($runningStateStream);

            $app = new Application();
            $app->add(new Generate());
            $app->setDefaultCommand('merge', true);
            $app->setAutoExit(false);
            $app->run($input, $output);

            $this->assertEquals(
                (string)$this->loadFixture('merged.whitelist.whitelist'),
                $output->fetch()
            );
        } finally {
            @fclose($runningStateStream);
        }
    }

    private function loadFixture(string $name): State
    {
        return (new Netfilter)->parseStateFromFile(__DIR__ . "/../Fixture/{$name}.state");
    }
}
