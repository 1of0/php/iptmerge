<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Merge;

use OneOfZero\IptMerge\Exception\NotImplementedException;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Merge\Configuration\Unmanaged;
use OneOfZero\IptMerge\Merge\StateMerger;
use OneOfZero\IptMerge\Merge\Strategy\RuleWhitelist;
use OneOfZero\IptMerge\Netfilter\Netfilter;
use OneOfZero\IptMerge\Netfilter\Structure\State;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;

class StateMergerTest extends TestCase
{
    /**
     * @param array       $behaviourConfig
     * @param string      $expectedStateName
     * @param string|null $generatedStateName
     *
     * @dataProvider basicMergeProvider
     */
    public function testMergeBasic(
        array $behaviourConfig,
        string $expectedStateName,
        ?string $generatedStateName = null
    ): void
    {
        $behaviour = MergeBehaviour::fromArray($behaviourConfig);

        $base       = $this->loadFixture('running');
        $supplicant = $this->loadFixture($generatedStateName ?? 'generated');

        $merger = new StateMerger($behaviour);

        echo "{$expectedStateName}\n";
        echo $merger->merge($base, $supplicant);
        echo "========\n";

        $this->assertEquals(
            (string)$this->loadFixture($expectedStateName),
            (string)$merger->merge($base, $supplicant)
        );
    }

    public function testBadMatchTarget(): void
    {
        $brokenBehaviour = MergeBehaviour::fromArray([]);
        $reflectionProperty = new ReflectionProperty(Unmanaged::class, 'ruleMatchAgainst');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($brokenBehaviour->getUnmanaged(), 'foo');

        $this->expectException(NotImplementedException::class);

        $strategy = new RuleWhitelist();
        $strategy->process(
            $this->loadFixture('running')->getTablesIndexedByName()['filter'],
            $this->loadFixture('generated')->getTablesIndexedByName()['filter'],
            $brokenBehaviour
        );
    }

    public function testBadChainStrategy(): void
    {
        $brokenBehaviour = MergeBehaviour::fromArray([]);
        $reflectionProperty = new ReflectionProperty(Unmanaged::class, 'chainStrategy');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($brokenBehaviour->getUnmanaged(), 'foo');

        $this->expectException(NotImplementedException::class);

        $stateMerger = new StateMerger($brokenBehaviour);
        $stateMerger->merge(
            $this->loadFixture('running'),
            $this->loadFixture('generated')
        );
    }

    public function testBadRuleStrategy(): void
    {
        $brokenBehaviour = MergeBehaviour::fromArray([]);
        $reflectionProperty = new ReflectionProperty(Unmanaged::class, 'ruleStrategy');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($brokenBehaviour->getUnmanaged(), 'foo');

        $this->expectException(NotImplementedException::class);

        $stateMerger = new StateMerger($brokenBehaviour);
        $stateMerger->merge(
            $this->loadFixture('running'),
            $this->loadFixture('generated')
        );
    }

    public function basicMergeProvider(): array
    {
        return [
            [
                [
                    'managed' => [
                        'chainPattern' => '^a-managed',
                        'rulePattern'  => '^b-managed',
                    ],
                ],
                'merged.keep.keep',
            ],
            [
                [
                    'managed' => [
                        'chainPattern' => '^a-managed',
                        'rulePattern'  => '^b-managed',
                    ],
                ],
                'merged.only-filter',
                'generated-only-filter',
            ],
            [
                [
                    'managed' => [
                        'chainPattern' => '^a-managed',
                        'rulePattern'  => '^b-managed',
                    ],
                    'unmanaged' => [
                        'chainStrategy' => Unmanaged::STRATEGY_PURGE,
                        'ruleStrategy'  => Unmanaged::STRATEGY_PURGE,
                    ],
                ],
                'merged.purge.purge',
            ],
            [
                [
                    'managed' => [
                        'chainPattern' => '^a-managed',
                        'rulePattern'  => '^b-managed',
                    ],
                    'unmanaged' => [
                        'chainStrategy'   => Unmanaged::STRATEGY_WHITELIST,
                        'chainWhitelists' => ['^foo'],
                    ],
                ],
                'merged.whitelist.keep',
            ],
            [
                [
                    'managed' => [
                        'chainPattern' => '^a-managed',
                        'rulePattern'  => '^b-managed',
                    ],
                    'unmanaged' => [
                        'ruleStrategy'   => Unmanaged::STRATEGY_WHITELIST,
                        'ruleWhitelists' => ['^foo'],
                    ],
                ],
                'merged.keep.whitelist',
            ],
            [
                [
                    'managed' => [
                        'chainPattern' => '^a-managed',
                        'rulePattern'  => '^b-managed',
                    ],
                    'unmanaged' => [
                        'chainStrategy'   => Unmanaged::STRATEGY_WHITELIST,
                        'chainWhitelists' => ['^foo'],
                        'ruleStrategy'    => Unmanaged::STRATEGY_WHITELIST,
                        'ruleWhitelists'  => ['^foo'],
                    ],
                ],
                'merged.whitelist.whitelist',
            ],
            [
                [
                    'managed' => [
                        'chainPattern' => '^a-managed',
                        'rulePattern'  => '^b-managed',
                    ],
                    'unmanaged' => [
                        'ruleMatchAgainst' => Unmanaged::MATCH_FULL,
                        'ruleStrategy'     => Unmanaged::STRATEGY_WHITELIST,
                        'ruleWhitelists'   => ['127\.1\.'],
                    ],
                ],
                'merged.full-rule-match',
            ],
        ];
    }

    private function loadFixture(string $name): State
    {
        return (new Netfilter)->parseStateFromFile(__DIR__ . "/../Fixture/{$name}.state");
    }
}
