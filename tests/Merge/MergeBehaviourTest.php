<?php

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/php/iptmerge/-/blob/master/LICENSE.md
 */

namespace OneOfZero\IptMerge\Test\Merge;

use OneOfZero\IptMerge\Exception\InvalidConfigurationException;
use OneOfZero\IptMerge\Merge\Configuration\MergeBehaviour;
use OneOfZero\IptMerge\Merge\Configuration\Unmanaged;
use PHPUnit\Framework\TestCase;

class MergeBehaviourTest extends TestCase
{
    /**
     * @param array       $inputStruct
     * @param string|null $expectedExceptionClass
     * @param string      $expectedMatchAgainst
     * @param string      $expectedChainStrategy
     * @param string      $expectedRuleStrategy
     * @param array       $expectedChainWhitelists
     * @param array       $expectedRuleWhitelists
     *
     * @dataProvider getterProvider
     */
    public function testGetters(
        array $inputStruct,
        ?string $expectedExceptionClass,
        string $expectedMatchAgainst = '',
        string $expectedChainStrategy = '',
        string $expectedRuleStrategy = '',
        array $expectedChainWhitelists = [],
        array $expectedRuleWhitelists = []
    ): void {
        if ($expectedExceptionClass !== null) {
            $this->expectException($expectedExceptionClass);
        }

        $mergeBehaviour = MergeBehaviour::fromArray($inputStruct);
        $this->assertEquals($expectedMatchAgainst, $mergeBehaviour->getUnmanaged()->getRuleMatchAgainst());
        $this->assertEquals($expectedChainStrategy, $mergeBehaviour->getUnmanaged()->getChainStrategy());
        $this->assertEquals($expectedRuleStrategy, $mergeBehaviour->getUnmanaged()->getRuleStrategy());
        $this->assertEquals($expectedChainWhitelists, $mergeBehaviour->getUnmanaged()->getChainWhitelists());
        $this->assertEquals($expectedRuleWhitelists, $mergeBehaviour->getUnmanaged()->getRuleWhitelists());
    }

    public function getterProvider(): array
    {
        return [
            [
                [
                    'unmanaged' => [
                        'ruleMatchAgainst' => Unmanaged::MATCH_COMMENT,
                    ],
                ],
                null,
                Unmanaged::MATCH_COMMENT,
                Unmanaged::STRATEGY_KEEP,
                Unmanaged::STRATEGY_KEEP,
            ],
            [
                [
                    'unmanaged' => [
                        'ruleMatchAgainst' => Unmanaged::MATCH_FULL,
                    ],
                ],
                null,
                Unmanaged::MATCH_FULL,
                Unmanaged::STRATEGY_KEEP,
                Unmanaged::STRATEGY_KEEP,
            ],
            [
                [
                    'unmanaged' => [
                        'ruleMatchAgainst' => 'foo',
                    ],
                ],
                InvalidConfigurationException::class,
            ],
            [
                [
                    'unmanaged' => [
                        'chainStrategy' => Unmanaged::STRATEGY_WHITELIST,
                    ],
                ],
                null,
                Unmanaged::MATCH_COMMENT,
                Unmanaged::STRATEGY_WHITELIST,
                Unmanaged::STRATEGY_KEEP,
            ],
            [
                [
                    'unmanaged' => [
                        'chainStrategy' => 'foo',
                    ],
                ],
                InvalidConfigurationException::class,
            ],
            [
                [
                    'unmanaged' => [
                        'ruleStrategy' => Unmanaged::STRATEGY_WHITELIST,
                    ],
                ],
                null,
                Unmanaged::MATCH_COMMENT,
                Unmanaged::STRATEGY_KEEP,
                Unmanaged::STRATEGY_WHITELIST,
            ],
            [
                [
                    'unmanaged' => [
                        'ruleStrategy' => 'foo',
                    ],
                ],
                InvalidConfigurationException::class,
            ],
            [
                [
                    'unmanaged' => [
                        'chainWhitelists' => ['foo', 'bar'],
                    ],
                ],
                null,
                Unmanaged::MATCH_COMMENT,
                Unmanaged::STRATEGY_KEEP,
                Unmanaged::STRATEGY_KEEP,
                ['foo', 'bar'],
            ],
            [
                [
                    'unmanaged' => [
                        'ruleWhitelists' => ['foo', 'bar'],
                    ],
                ],
                null,
                Unmanaged::MATCH_COMMENT,
                Unmanaged::STRATEGY_KEEP,
                Unmanaged::STRATEGY_KEEP,
                [],
                ['foo', 'bar'],
            ],
            [
                [
                    'unmanaged' => [
                        'ruleMatchAgainst' => Unmanaged::MATCH_FULL,
                        'chainStrategy' => Unmanaged::STRATEGY_WHITELIST,
                        'ruleStrategy' => Unmanaged::STRATEGY_WHITELIST,
                        'chainWhitelists' => ['foo', 'bar'],
                        'ruleWhitelists' => ['baz'],
                    ],
                ],
                null,
                Unmanaged::MATCH_FULL,
                Unmanaged::STRATEGY_WHITELIST,
                Unmanaged::STRATEGY_WHITELIST,
                ['foo', 'bar'],
                ['baz'],
            ],
        ];
    }
}
