# 1of0/iptmerge

[![pipeline status](https://gitlab.com/1of0/php/iptmerge/badges/master/pipeline.svg)](https://gitlab.com/1of0/php/iptmerge/-/commits/master)
[![coverage report](https://gitlab.com/1of0/php/iptmerge/badges/master/coverage.svg)](https://gitlab.com/1of0/php/iptmerge/-/commits/master)

## What

A tool to merge two `iptables-restore` compatible dumps.

## License

This project is licensed under the MIT license. See [LICENSE.md](LICENSE.md).
